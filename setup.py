from setuptools import setup, find_packages

setup(name='ozon_db',
      version='0.0.1',
      description='DB wrapper for ozon api',
      long_description='DB wrapper for ozon api',
      classifiers=[
          'Programming Language :: Python :: 3.10',
      ],
     # url='https://gitlab.com/ajvarnabiullin/ozon-sdk-python',
      author='Aivar Nabiullin',
      author_email='ajvarnabiullin0@gmail.com',
      packages=find_packages(),
      # include_package_data=False,
      zip_safe=False)

