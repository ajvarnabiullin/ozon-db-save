import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

def create_db():
    # Устанавливаем соединение с postgres
    connection = psycopg2.connect(user="postgres", password="postgres")
    connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    # Создаем курсор для выполнения операций с базой данных
    cursor = connection.cursor()
    # sql_create_database = 
    # Создаем базу данных
    cursor.execute('create database ozon_db3')
    # Закрываем соединение
    cursor.close()
    connection.close()