from contextlib import contextmanager
from typing import ContextManager
from sqlalchemy import create_engine


from sqlalchemy.engine.url import URL
from sqlalchemy.orm import Session, sessionmaker
from ..create_db import create_db
# from sqlmodel import create_engine, SQLModel, select
from orm.base import DeclarativeBase


class DBConnection:

    def __init__(self):
        # create_db()
        self._engine  = create_engine('postgresql+psycopg2://postgres:postgres@localhost:5432/ozon_db3')
        self._engine.connect()
        DeclarativeBase.metadata.create_all(self._engine)
        self._session = Session(bind=self._engine)


class DBContext(DBConnection):
    
    @contextmanager
    def session(self) -> ContextManager[Session]:
        session: Session = self._scoped_session_factory()
        try:
            yield session
        except Exception:
            session.rollback()
            raise
        finally:
            session.close()

    def add(self, model):
        print(model.wh_items[0].name)
        print(model.wh_items[0].custom_id)

        print(self._session.add(model))
        self._session.commit()
        self._session.refresh(model)
        print('Model')
        print(model)
        print(self._session.query(type(model)).all())
 
        # for i in self._session.query(type(model)).all():
        #     print(i)

       
        return self._session