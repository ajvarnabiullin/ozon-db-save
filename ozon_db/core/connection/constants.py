DATABASE = {
    'drivername': 'postgres', #Тут можно использовать MySQL или другой драйвер
    'host': 'localhost',
    'port': '5432',
    'username': 'postgres',
    'password': 'postgres',
    'database': 'ozon_db'
}