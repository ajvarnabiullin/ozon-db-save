from typing import Optional, TypeVar, Generic, Type
from orm.base import DeclarativeBase
from core.connection import DBContext

TEntity = TypeVar('TEntity', bound=DeclarativeBase)

class DBWrapper(Generic[TEntity]):
    """Обертка для работы с моделями"""

    def __init__(self, db_context: DBContext,  entity_type: Type(TEntity)):
        self.db_context = db_context
        self.entity_type = entity_type

    def add(self, entity: TEntity) -> None:
        with self._db_context.session() as s:
            s.add(entity)
            s.commit()
            s.refresh(entity)


    def get_by_id(self, entity_id) -> Optional[TEntity]:
        """
        :type entity_id: TEntity.id
        :return:
        """
        with self._db_context.session() as s:
            entity = s.query(self._entity_type).get(entity_id)
        return entity

    def remove(self, entity: TEntity) -> None:
        with self._db_context.session() as s:
            s.query(self._entity_type).where(self._entity_type.id == entity.id).delete()
            s.commit()

    def add_many(self, entities: list[TEntity]) -> None:
        with self._db_context.session() as s:
            s.add_all(entities)
            s.commit()

    def update(self, entity: TEntity) -> None:
        with self._db_context.session() as s:
            s.query(self._entity_type).where(self._entity_type.id == entity.id).update(entity.dict())
            s.commit()

