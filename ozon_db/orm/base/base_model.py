from typing import Optional
from sqlmodel import Field, SQLModel, create_engine
from sqlalchemy.ext.declarative import declarative_base

class BaseModel(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)

DeclarativeBase = declarative_base()

# class BaseModel(DeclarativeBase):
#     pass