from itertools import product
from ozon_sdk.response import AnalyticsStockOnWarehouseResponse
from ozon_sdk.entities import TotalItem, WhItem
from ozon_sdk.entities.total_item import Stock as total_item_stock
from ozon_sdk.entities.wh_item import Item as wh_item_item
from ozon_sdk.entities.wh_item import Stock as wh_item_stock
from typing import Type, TypeVar

from sqlalchemy.ext.declarative import declarative_base

from .models import AnalyticsStockOnWarehouseModel, AnalyticsTotalItemModel, AnalyticsWhItemModel, AnalyticsWhItemItemModel, AnalyticsTotalItemStocksModel, AnalyticsWhItemItemStockModel
from datetime import datetime
import pydantic

TBaseModel = TypeVar('TBaseModel', bound=declarative_base())
TResponse = TypeVar('TResponse', bound=pydantic.BaseModel)

class ComparisonsSchemesToModels:

    comparisons_schemes_to_models: dict[Type[TResponse], Type[declarative_base()]] = {
        AnalyticsStockOnWarehouseResponse: AnalyticsStockOnWarehouseModel,
        TotalItem: AnalyticsTotalItemModel,
        WhItem: AnalyticsWhItemModel,
        wh_item_item: AnalyticsWhItemItemModel,
        total_item_stock: AnalyticsTotalItemStocksModel,
        wh_item_stock: AnalyticsWhItemItemStockModel,


    }
    str_types = [str, int, bool, float, datetime]

    def assert_necessary_field(self, entity: TBaseModel, response_fields: dict, response: TResponse):

        for response_field, model_field in product(response_fields, type(entity).__dict__):

            if model_field != response_field: 
                continue
            
            if (response_fields[response_field]) in ComparisonsSchemesToModels.str_types:
                setattr(entity, model_field, getattr(response, response_field))
                continue

            if type(response_fields[response_field]) == dict:
                setattr(entity, model_field, self.assert_necessary_field(entity, getattr(response, response_field), response))
                continue
            if response_fields[response_field].__mro__ == (list, object):
                """Через рекурсию сохранять"""
                for obj in getattr(response, response_field):
                    nested_instance = ComparisonsSchemesToModels.comparisons_schemes_to_models[type(obj)]
                    nested_entity = nested_instance()
                    en = self.assert_necessary_field(nested_entity, type(obj).__dict__['__annotations__'], obj)
                    getattr(entity, response_field).append(en)
        return entity

    def parser_response_to_model(self, response: TResponse) -> TBaseModel:

        entity_type = ComparisonsSchemesToModels.comparisons_schemes_to_models[type(response)]
        entity = entity_type()
        response_fields = type(response).__dict__['__annotations__']

        entity = self.assert_necessary_field( entity,response_fields, response)

        return entity
