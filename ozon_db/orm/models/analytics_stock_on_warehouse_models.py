from sqlalchemy import Integer, String, \
    Column, DateTime, ForeignKey, Numeric, Float, ARRAY
from sqlalchemy.orm import relationship
from datetime import datetime
from orm.base import DeclarativeBase


class AnalyticsTotalItemStocksModel(DeclarativeBase):
    """Информация об остатках товаров."""
    __tablename__ = 'analytics_total_item_stock'

    id = Column(Integer, primary_key=True)
    between_warehouses = Column(Integer, default=0)
    for_sale = Column(Integer, default=0)
    loss = Column(Integer, default=0)
    not_for_sale = Column(Integer, default=0)
    shipped = Column(Integer, default=0)

    analytics_total_item_id = Column(Integer, ForeignKey('analytics_total_item.id'))

    


class AnalyticsTotalItemModel(DeclarativeBase):
    """Данные по остаткам на всех складах."""
    __tablename__ = 'analytics_total_item'
    
    id = Column(Integer, primary_key=True)
    barcode = Column(String, default='')
    category = Column(String, default='')
    discounted = Column(String, default='')
    height = Column(Float, default=0)
    length = Column(Float, default=0)
    offer_id = Column(String, default='')
    sku = Column(String, default='')
    title = Column(String, default='')
    volume = Column(Float, default=0)
    weight = Column(Float, default=0)
    width = Column(Float, default=0)

    stocks = relationship('AnalyticsTotalItemStocksModel', uselist=False, cascade="all, delete-orphan")
    analytics_stock_on_warehouse_id = Column(Integer, ForeignKey('analytics_stock_on_warehouse.id'))




class AnalyticsWhItemItemStockModel(DeclarativeBase):
    """object (AnalyticsGetStockOnWarehousesResponseItemStock)"""
    __tablename__ = 'analytics_wh_item_item_stock'

    id = Column(Integer, primary_key=True)
    for_sale = Column(Integer, default=0)
    loss = Column(Integer, default=0)
    not_for_sale = Column(Integer, default=0)


    analytics_wh_item_item_id = Column(Integer, ForeignKey('analytics_wh_item_item.id'))


class AnalyticsWhItemItemModel(DeclarativeBase):
    """Информация о товарах на складе."""
    __tablename__ = 'analytics_wh_item_item'

    id = Column(Integer, primary_key=True)
    barcode = Column(String, default='')
    category = Column(String, default='')
    discounted = Column(String, default='')
    height = Column(Float, default=0)
    length = Column(Float, default=0)
    offer_id = Column(String, default='')
    sku = Column(String, default='')
    title = Column(String, default='')
    volume = Column(Float, default=0)
    weight = Column(Float, default=0)
    width = Column(Float, default=0)

    stocks = relationship('AnalyticsWhItemItemStockModel', uselist=False, cascade="all, delete-orphan")
    analytics_wh_item_id = Column(Integer, ForeignKey('analytics_wh_item.id'), default=None)



class AnalyticsWhItemModel(DeclarativeBase):
    """Данные остатков по определённым складам."""
    __tablename__ = 'analytics_wh_item'

    id = Column(Integer, primary_key=True)
    custom_id = Column(String, default='')
    name = Column(String, default='')

    items = relationship('AnalyticsWhItemItemModel', cascade="all, delete-orphan")
    analytics_stock_on_warehouse_id = Column(Integer, ForeignKey('analytics_stock_on_warehouse.id'), default=None)

class AnalyticsStockOnWarehouseModel(DeclarativeBase):
    """Отчёт по остаткам и товарам"""
    __tablename__ = 'analytics_stock_on_warehouse'

    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime(), default=datetime.now)
    total_items = relationship('AnalyticsTotalItemModel', cascade="all, delete-orphan")
    wh_items = relationship('AnalyticsWhItemModel', cascade="all, delete-orphan")







# from collections import UserList
# from email.policy import default
# from lib2to3.pgen2.token import OP
# # from orm.base import DeclarativeBase
# # from core.connection import DeclarativeBase
# from sqlalchemy import create_engine, MetaData, Table, Integer, String, \
#     Column, DateTime, ForeignKey, Numeric, Float, ARRAY
# from sqlalchemy.orm import relationship
# from datetime import datetime
# from orm.base import BaseModel
# from sqlmodel import Relationship, Field, SQLModel
# from typing import Optional, List

# class Team(SQLModel, table=True):
#     id: Optional[int] = Field(default=None, primary_key=True)
#     name: str = Field(index=True)
#     headquarters: str

#     heroes: List["Hero"] = Relationship(back_populates="team")


# class Hero(SQLModel, table=True):
#     id: Optional[int] = Field(default=None, primary_key=True)
#     name: str = Field(index=True)
#     secret_name: str
#     age: Optional[int] = Field(default=None, index=True)

#     team_id: Optional[int] = Field(default=None, foreign_key="team.id")
#     team: Optional[Team] = Relationship(back_populates="heroes")


# class AnalyticsTotalItemStocksModel(SQLModel, table=True):
#     """Информация об остатках товаров."""
#     __tablename__ = 'analytics_total_item_stock'

#     id: Optional[int] = Field(default=None, primary_key=True)
#     between_warehouses: int = 0
#     for_sale: int = 0
#     loss: int = 0
#     not_for_sale: int = 0
#     shipped: int = 0

#     analytics_total_item: Optional['AnalyticsTotalItemModel'] = Relationship(back_populates='stocks')
    


# class AnalyticsTotalItemModel(SQLModel, table=True):
#     """Данные по остаткам на всех складах."""
#     __tablename__ = 'analytics_total_item'
    
#     id: Optional[int] = Field(default=None, primary_key=True)
#     barcode: str = ''
#     category: str = ''
#     discounted : str = ''
#     height: float = 0
#     length: float = 0
#     offer_id: str = ''
#     sku: str = ''
#     title: str = ''
#     volume: float = 0
#     weight: float = 0
#     width: float = 0

#     stocks: AnalyticsTotalItemStocksModel = Relationship(back_populates='analytics_total_item')
#     analytics_stock_on_warehouse_id: Optional[int] = Field(default=None, foreign_key="analytics_stock_on_warehouse.id")

#     analytics_stock_on_warehouse: Optional['AnalyticsStockOnWarehouseModel'] = Relationship(back_populates='total_item')




# class AnalyticsWhItemItemStockModel(SQLModel, table=True):
#     """object (AnalyticsGetStockOnWarehousesResponseItemStock)"""
#     __tablename__ = 'analytics_wh_item_item_stock'

#     id: Optional[int] = Field(default=None, primary_key=True)
#     for_sale: int = 0
#     loss: int = 0
#     not_for_sale: int = 0

#     # analytics_wh_item_item_id: Optional[int] = Field(foreign_key="analytics_wh_item_item.id")
#     analytics_wh_item_item: Optional['AnalyticsWhItemItemModel'] = Relationship(back_populates='stocks', sa_relationship_kwargs={'uselist': False})


# class AnalyticsWhItemItemModel(SQLModel, table=True):
#     """Информация о товарах на складе."""
#     __tablename__ = 'analytics_wh_item_item'

#     id: Optional[int] = Field(default=None, primary_key=True)
#     barcode: str = ''
#     category: str = ''
#     discounted: str = ''
#     height: float = 0
#     length: float = 0
#     offer_id: str = ''
#     sku: str = ''
#     title: str = ''
#     volume: float = 0
#     weight: float = 0
#     width: float = 0

#     stocks: Optional[AnalyticsWhItemItemStockModel] = Relationship(back_populates='analytics_wh_item_item', )
#     stocks_id: Optional[int] = Field(foreign_key="analytics_wh_item_item_stock.id",)
#     # stocks_id: Optional[]
#     analytics_wh_item: Optional['AnalyticsWhItemModel'] = Relationship(back_populates='items')


# class AnalyticsWhItemModel(SQLModel, table=True):
#     """Данные остатков по определённым складам."""
#     __tablename__ = 'analytics_wh_item'

#     id: Optional[int] = Field(default=None, primary_key=True)
#     custom_id: str = ''
#     name: str = ''

#     analytics_stock_on_warehouse: Optional['AnalyticsStockOnWarehouseModel'] = Relationship(back_populates='wh_items')

#     items: List[AnalyticsWhItemItemModel] = Relationship(back_populates='analytics_wh_item')

# class AnalyticsStockOnWarehouseModel(SQLModel, table=True):
#     """Отчёт по остаткам и товарам"""
#     __tablename__ = 'analytics_stock_on_warehouse'

#     id: Optional[int] = Field(default=None, primary_key=True)
#     timestamp: datetime = datetime.now
#     total_items: List[AnalyticsTotalItemModel] = Relationship(back_populates="analytics_stock_on_warehouse")
#     wh_items: List[AnalyticsWhItemModel] = Relationship(back_populates='analytics_stock_on_warehouse')
#     random: int




