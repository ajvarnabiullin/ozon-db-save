from collections import UserList
from email.policy import default
from lib2to3.pgen2.token import OP
# from orm.base import DeclarativeBase
from core.connection import DeclarativeBase
from sqlalchemy import create_engine, MetaData, Table, Integer, String, \
    Column, DateTime, ForeignKey, Numeric, Float, ARRAY
from sqlalchemy.orm import relationship
from datetime import datetime
from orm.base import BaseModel
from sqlmodel import Relationship
from typing import Optional, List

class AnalyticsTotalItemStocksModel(BaseModel):
    """Информация об остатках товаров."""
    __tablename__ = 'analytics_total_item_stock'

    between_warehouses: int = 0
    for_sale: int = 0
    loss: int = 0
    not_for_sale: int = 0
    shipped: int = 0

    analytics_total_item: Optional['AnalyticsTotalItemModel'] = Relationship(back_populates='stocks')
    


class AnalyticsTotalItemModel(BaseModel):
    """Данные по остаткам на всех складах."""
    __tablename__ = 'analytics_total_item'
    
    barcode: str = ''
    category: str = ''
    discounted : str = ''
    height: float = 0
    length: float = 0
    offer_id: str = ''
    sku: str = ''
    title: str = ''
    volume: float = 0
    weight: float = 0
    width: float = 0

    stocks: AnalyticsTotalItemStocksModel = Relationship(back_populates='analytics_total_item')

    analytics_stock_on_warehouse: Optional['AnalyticsStockOnWarehouseModel'] = Relationship(back_populates='total_item')




class AnalyticsWhItemItemStockModel(BaseModel):
    """object (AnalyticsGetStockOnWarehousesResponseItemStock)"""
    __tablename__ = 'analytics_wh_item_item_stock'

    for_sale: int = 0
    loss: int = 0
    not_for_sale: int = 0


    analytics_wh_item_item_id: Optional['AnalyticsWhItemItemModel'] = Relationship(back_populates=stocks)


class AnalyticsWhItemItemModel(BaseModel):
    """Информация о товарах на складе."""
    __tablename__ = 'analytics_wh_item_item'

    barcode: str
    category: str
    discounted: str
    height: float
    length: float
    offer_id: str
    sku: str
    title: str
    volume: float
    weight: float
    width: float

    stocks: AnalyticsWhItemItemStockModel = Relationship(back_populates='analytics_wh_item_item')
    analytics_wh_item: Optional['AnalyticsWhItemModel'] = Relationship(back_populates='items')





class AnalyticsWhItemModel(BaseModel):
    """Данные остатков по определённым складам."""
    __tablename__ = 'analytics_wh_item'

    custom_id = Column(String, default='')
    name: str = ''

    analytics_stock_on_warehouse: Optional['AnalyticsStockOnWarehouseModel'] = Relationship(back_populates='wh_items')

    items: List[AnalyticsWhItemItemModel] = Relationship(back_populates='analytics_wh_item')

class AnalyticsStockOnWarehouseModel(BaseModel):
    """Отчёт по остаткам и товарам"""
    __tablename__ = 'analytics_stock_on_warehouse'

    timestamp: datetime = datetime.now
    total_items: List[AnalyticsTotalItemModel] = Relationship(back_populates="analytics_stock_on_warehouse")
    wh_items: List[AnalyticsWhItemModel] = Relationship(back_populates='analytics_stock_on_warehouse')





